require('dotenv').config();

var AWS = require("aws-sdk");

AWS.config.update({
  region: process.env.REACT_APP_REGION,
  accessKeyId: process.env.REACT_APP_AWS_ACCESS_ID,
  secretAccessKey: process.env.REACT_APP_AWS_SECRET_ACCESS_KEY
});

var docClient = new AWS.DynamoDB.DocumentClient();

const express = require('express')
const cors = require('cors')
const app = express()
const port= process.env.PORT || 5000 //assigns own port. Do or so works on heroku and local

app.use(express.json());
app.use(cors());

app.get('/', (req, res) => {
  res.send('Hello World!')
})

//contact.jsx:10          POST http://localhost:3000/undefined 404 (Not Found)
// SyntaxError: Unexpected token < in JSON at position 0
app.post("/", function(req,res) {
    var params = {
      TableName:process.env.REACT_APP_TABLE_NAME,
      Item:{
        "email_address":req.body.email_address,
        "message":req.body.message
      }
    };
    docClient.put(params, function(err, data) {
      if (err) {
          console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
          res.status(400).send();
      } else {
          console.log("Added item!");
          res.status(200).send();
      }
  });
})

app.listen(port, () => {
  console.log(`Backend listening on port ${port}`)
})

/**
 * contact.jsx:11          POST http://localhost:5000/ net::ERR_CONNECTION_RESET
handleSubmit @ contact.jsx:11
callCallback @ react-dom.development.js:3945
invokeGuardedCallbackDev @ react-dom.development.js:3994
invokeGuardedCallback @ react-dom.development.js:4056
invokeGuardedCallbackAndCatchFirstError @ react-dom.development.js:4070
executeDispatch @ react-dom.development.js:8243
processDispatchQueueItemsInOrder @ react-dom.development.js:8275
processDispatchQueue @ react-dom.development.js:8288
dispatchEventsForPlugins @ react-dom.development.js:8299
(anonymous) @ react-dom.development.js:8508
batchedEventUpdates$1 @ react-dom.development.js:22396
batchedEventUpdates @ react-dom.development.js:3745
dispatchEventForPluginEventSystem @ react-dom.development.js:8507
attemptToDispatchEvent @ react-dom.development.js:6005
dispatchEvent @ react-dom.development.js:5924
unstable_runWithPriority @ scheduler.development.js:468
runWithPriority$1 @ react-dom.development.js:11276
discreteUpdates$1 @ react-dom.development.js:22413
discreteUpdates @ react-dom.development.js:3756
dispatchDiscreteEvent @ react-dom.development.js:5889
contact.jsx:29 TypeError: Failed to fetch
    at handleSubmit (contact.jsx:11:1)
    at HTMLUnknownElement.callCallback (react-dom.development.js:3945:1)
    at Object.invokeGuardedCallbackDev (react-dom.development.js:3994:1)
    at invokeGuardedCallback (react-dom.development.js:4056:1)
    at invokeGuardedCallbackAndCatchFirstError (react-dom.development.js:4070:1)
    at executeDispatch (react-dom.development.js:8243:1)
    at processDispatchQueueItemsInOrder (react-dom.development.js:8275:1)
    at processDispatchQueue (react-dom.development.js:8288:1)
    at dispatchEventsForPlugins (react-dom.development.js:8299:1)
    at react-dom.development.js:8508:1
 */